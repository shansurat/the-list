import {
    MDCTextField
} from '@material/textfield'
import {
    MDCRipple
} from '@material/ripple'
import {
    MDCTopAppBar
} from '@material/top-app-bar/index';
import {
    MDCDialog
} from '@material/dialog';
import {
    MDCDrawer
} from "@material/drawer";
import {
    MDCTextFieldIcon
} from '@material/textfield/icon';
import {
    MDCSnackbar
} from '@material/snackbar';

import {
    MDCLinearProgress
} from '@material/linear-progress'

let progressbar

const $ = require('jquery')

$(() => {
    const drawer = new MDCDrawer($('#drawer')[0])
    const navbar = new MDCTopAppBar($('#nav-bar')[0])
    progressbar = new MDCLinearProgress($('#progressbar')[0])


    navbar.listen('MDCTopAppBar:nav', () => drawer.open = !drawer.open)

    $('#search-toggler').click(() => {
        $('#new-filter').toggle('fast')
        if ($('#new-filter').is(':hidden')) {
            $('#new-filter__field').val('')
            $('.selected').toggleClass('selected')
        }
    })
    switch (sessionStorage.page) {
        case 'auth':
            loadAuthPage()
            break

        case 'list':
            loadListPage()
            break

        default:
            $.post('services/session.service.php', (page) => {
                sessionStorage.page = page
                page == 'auth' ? loadAuthPage() : loadListPage()
            })
    }
})

function loadAuthPage() {
    sessionStorage.page = 'auth'
    $('#nav-bar').slideUp()
    $('#list-methods').hide()

    $('#main-content').load('auth.component.php', () => {
        MDCTextField.attachTo($('#username')[0])
        MDCTextField.attachTo($('#password')[0])
        MDCRipple.attachTo($('#auth-button')[0])
        MDCTextFieldIcon.attachTo($('#password-toggler')[0])
        const wrong_credentials = new MDCSnackbar($('#wrong-credentials')[0])
        const credentials_hint = new MDCDialog($('#credentials-hint')[0])

        $('#password-toggler').click((e) => {
            $(e.target).text($(e.target).text() == 'visibility' ? 'visibility_off' : 'visibility')
            $('#password-text-field').attr('type', $(e.target).text() == 'visibility' ? 'text' : 'password')
        })

        $('#credentials-hint__button').click(() => credentials_hint.open())

        $('#auth-form').submit((e) => {
            e.preventDefault()
            $.ajax({
                url: 'services/auth.service.php',
                type: 'post',
                dataType: 'json',
                data: $('#auth-form').serialize(),
                success: (res) => {
                    if (!!res.authenticated)
                        loadListPage()
                    else {
                        $('#username-text-field').val('')
                        $('#password-text-field').val('')
                        wrong_credentials.open()
                    }
                },
                error: (jqXHR, err, mess) => console.error(mess)

            })
        })
    })

}

function loadListPage() {
    sessionStorage.page = 'list'
    $('#list-methods').show()
    $('#nav-bar').fadeIn()

    $('#main-content').load('list.component.php', () => {
        MDCRipple.attachTo($('#new-resource__button')[0])
        MDCRipple.attachTo($('#edit-resource__button')[0])
        MDCRipple.attachTo($('#delete-resource__button')[0])
        MDCTextField.attachTo($('#nr__name')[0])
        MDCTextField.attachTo($('#nr__creator')[0])
        MDCTextField.attachTo($('#nr__type')[0])
        MDCTextField.attachTo($('#er__name')[0])
        MDCTextField.attachTo($('#er__creator')[0])
        MDCTextField.attachTo($('#er__type')[0])
        MDCTextField.attachTo($('#new-filter')[0])

        $('td').each((i, el) => MDCRipple.attachTo(el))

        let new_resource__dialog = new MDCDialog($('#new-resource__dialog')[0])
        let edit_resource__dialog = new MDCDialog($('#edit-resource__dialog')[0])

        edit_resource__dialog.listen('MDCDialog:opened', () => {
            let cur_name = $('tr.selected').find('.name').text()
            let cur_creator = $('tr.selected').find('.creator').text()
            let cur_type = $('tr.selected').find('.type').text()

            $('#er__name>input').val(cur_name)
            $('#er__creator>input').val(cur_creator)
            $('#er__type>input').val(cur_type)

            $('#edit-resource__form').keyup(() => {
                $('#edit-resource__submit').attr('disabled', $('#er__name>input').val() == cur_name && $('#er__creator>input').val() == cur_creator && $('#er__type>input').val() == cur_type)
            })
        })

        new_resource__dialog.listen('MDCDialog:opened', () => {
            $('#new-resource__form>.mdc-text-field>input').keyup(() => $('#new-resource__submit').attr('disabled', !($('#nr__name>input').val() && $('#nr__creator>input').val() && $('#nr__type>input').val())))
        })

        new_resource__dialog.listen('MDCDialog:closed', () => {
            $('#new-resource__form>.mdc-text-field>input').val('')
        })


        $.ajax({
            url: 'services/list.service.php',
            type: 'post',
            dataType: 'json',
            beforeSend: () => progressbar.open(),
            success: (list) => {
                list.forEach(item => {

                    $('#list').append(generateRow(item))
                })

                document.querySelectorAll('tbody>tr').forEach(row => MDCRipple.attachTo(row))

                loadResourceModifiers()
            },
            error: (jqXHR, err, mess) => console.error(mess),
            complete: () => progressbar.close()
        })

        $('#auth-out__button').click(() => $.post('services/auth.service.php', () => loadAuthPage()))

        $('#new-resource__button').click(() => new_resource__dialog.open())
        $('#new-resource__submit').click(() => $('#new-resource__form').submit())

        $('#new-resource__form').submit((e) => {
            e.preventDefault()
            $.ajax({
                url: 'services/new-resource.service.php',
                type: 'post',
                dataType: 'json',
                data: $('#new-resource__form').serialize(),
                success: (res) => {
                    $('#list').append(generateRow(res[0]))
                },
                error: (jqXHR, err, mess) => console.error(mess)
            })
        })

        $('#delete-resource__button').click(() => {
            let ids = []

            $('tr.selected').each((i, el) => ids.push($(el).attr('id').split('_')[1]))

            $.ajax({
                url: 'services/delete-resource.service.php',
                type: 'post',
                data: {
                    ids: ids
                },
                success: (res) => {
                    ids.forEach((id) => $('#resource_' + id).hide().remove())
                    loadResourceModifiers()
                },
                error: (jqXHR, err, mess) => console.error(mess)
            })
        })

        $('#edit-resource__button').click(e => edit_resource__dialog.open())
        $('#edit-resource__submit').click(() => $('#edit-resource__form').submit())

        $('#edit-resource__form').submit(e => {
            e.preventDefault()

            let id = $('.selected').attr('id').split('_')[1]
            let edited = {}
            $($('#edit-resource__form').serializeArray()).each((i, el) => edited[el.name] = el.value)

            $.ajax({
                url: 'services/edit-resource.service.php',
                type: 'post',
                data: $('#edit-resource__form').serialize() + '&id=' + id,
                success: () => {
                    $('tr.selected').find('.name').text(edited.name)
                    $('tr.selected').find('.creator').text(edited.creator)
                    $('tr.selected').find('.type').text(edited.type)
                    $('tr.selected').toggleClass('selected')
                },

                error: (jqXHR, err, mess) => console.error(mess)
            })
        })

        let $cst = $('#case-sensitivity_toggle')

        $('#new-filter__field')
            .keyup((e) => {
                let filter = $(e.target).val()

                $('tbody>tr').each((i, row) => {
                    let filter_passed = false
                    $(row).find('td').each((i, cell) => {
                        let text = $cst.hasClass('enabled') ? $(cell).text() : $(cell).text().toLowerCase()
                        if (text.search($cst.hasClass('enabled') ? filter : filter.toLowerCase()) != -1) {
                            filter_passed = true

                            return false
                        }
                    })

                    filter_passed ? $(row).show() : $(row).hide()
                })

                highlighter(filter, $cst.hasClass('enabled'))
            })
            .focusin(() => highlighter($('#new-filter__field').val(), $cst.hasClass('enabled')))
            .focusout(() => $('mark').each((i, el) => $(el).parent().text($(el).parent().text())))


        $cst.click(() => {
            $cst.toggleClass('enabled')

            $('#new-filter>label>span').text($cst.hasClass('enabled') ? 'Yes' : 'No')
            highlighter($('#new-filter__field').val(), $cst.hasClass('enabled'))
        })
    })
}

function generateRow(resource) {
    return $('<tr/>', {
            id: 'resource_' + resource.id,
        })
        .append($('<td/>').text(resource.name).addClass('name mdc-typography--body1'))
        .append($('<td/>').text(resource.creator).addClass('creator mdc-typography--body1'))
        .append($('<td/>').text(resource.type).addClass('type mdc-typography--body1'))
        .click(e => {
            $(e.target).parent().toggleClass('selected')
            loadResourceModifiers()
        })
}

function loadResourceModifiers() {
    $('#delete-resource__button>.mdc-fab__label').text(
        $('tr.selected').length == 1 ?
        'Delete' :
        'Delete all (' + $('.selected').length + ')'
    )

    if (!$('tr.selected').length) {
        $('#edit-resource__button, #delete-resource__button').hide()
        $('#new-resource__button').show()
    } else if ($('tr.selected').length == 1) {
        $('#new-resource__button').hide()
        $('#delete-resource__button>.mdc-fab__icon').text('delete')
        $('#edit-resource__button, #delete-resource__button').show()
    } else {
        $('#new-resource__button, #edit-resource__button').hide()
        $('#delete-resource__button>.mdc-fab__icon').text('delete_sweep')
        $('#delete-resource__button').show()
    }
}

function highlighter(filter, cst) {
    $('mark').each((i, el) => $(el).parent().text($(el).parent().text()))

    $('tbody>tr').each((i, row) => {
        $(row).find('td').each((i, cell) => {
            let text = cst ? $(cell).text() : $(cell).text().toLowerCase()
            if (text.search(filter.toLowerCase()) != -1) {
                let searched
                filter.length == 1 ?
                    searched = $(cell).text()[text.toLowerCase().indexOf(filter.toLowerCase())] :

                    searched = $(cell).text().substring(
                        text.toLowerCase().indexOf(filter.toLowerCase()), filter.length
                    )

                $(cell).html($(cell).text().replace(searched, '<mark>' + searched + '</mark>'))
            }


        })
    })
}