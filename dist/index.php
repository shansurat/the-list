<?php

session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>The
        List<?php echo isset($_SESSION['authenticated']) && $_SESSION['authenticated'] ? '' : ' | Authentication' ?>
    </title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
</head>

<body class="mdc-typography">
    <aside id="drawer" class="mdc-drawer mdc-drawer--modal">
        <div class="mdc-drawer__header">
            <h3 class="mdc-drawer__title">Creators</h3>
            <h6 class="mdc-drawer__subtitle">#mgapakana</h6>
        </div>
        <div class="mdc-drawer__content">
            <button class="mdc-button">
                <span class="mdc-button__label">Kristian Mark Surat</span>
            </button>
            <button class="mdc-button">
                <span class="mdc-button__label">Misty Vito</span>
            </button>
        </div>
    </aside>
    <div class="mdc-drawer-scrim"></div>

    <header id="nav-bar" class="mdc-top-app-bar mdc-top-app-bar--fixed" style="display: none">
        <div id="progressbar" role="progressbar" class="mdc-linear-progress mdc-linear-progress--indeterminate">
            <div class="mdc-linear-progress__buffering-dots"></div>
            <div class="mdc-linear-progress__buffer"></div>
            <div class="mdc-linear-progress__bar mdc-linear-progress__primary-bar">
                <span class="mdc-linear-progress__bar-inner"></span>
            </div>
            <div class="mdc-linear-progress__bar mdc-linear-progress__secondary-bar">
                <span class="mdc-linear-progress__bar-inner"></span>
            </div>
        </div>

        <div class="mdc-top-app-bar__row">
            <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
                <a id="drawer-toggler" class="material-icons mdc-top-app-bar__navigation-icon">menu</a>
                <span class="mdc-top-app-bar__title">The List</span>
            </section>
            <section id="list-methods" class="mdc-top-app-bar__section mdc-top-app-bar__section--align-end">
                <a id="search-toggler" class="material-icons mdc-top-app-bar__navigation-icon">search</a>
                <a id="auth-out__button" class="material-icons mdc-top-app-bar__navigation-icon">exit_to_app</a>
            </section>
        </div>
    </header>

    <div id='main-content' class='mdc-top-app-bar--fixed-adjust'>

    </div>


</body>

</html>