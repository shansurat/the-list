<?php

require '../classes/dbconn.class.php';

$dbconn = new dbConnect();

$dbconn->openConnection();

$res = mysqli_query($dbconn->connect, 'SELECT * FROM sources;');

echo json_encode(mysqli_fetch_all($res, MYSQLI_ASSOC));

$dbconn->closeConnection();
