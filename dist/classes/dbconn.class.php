<?php

class dbConnect
{

    private $hostname;
    private $username;
    private $password;
    private $databasename;

    public $connect;
    private $selectDB;

    public function __construct()
    {
        $this->hostname = "localhost";
        $this->username = "id9557908_admin";
        $this->password = "password";
        $this->databasename = "id9557908_the_list";
    }

    public function openConnection()
    {
        $this->connect = mysqli_connect($this->hostname, $this->username, $this->password);

        if ($this->connect) {
            $this->selectDB();
        }

        return $this->connect;
    }

    public function selectDB()
    {
        $this->selectDB = mysqli_select_db($this->connect, $this->databasename);

        return $this->selectDB;
    }

    public function closeConnection()
    {
        mysqli_close($this->connect);
    }
}
