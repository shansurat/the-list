<h1 class="mdc-typography--headline1" style="text-align: center;">Authentication</h1>

<form id="auth-form">
    <div id="username" class="mdc-text-field mdc-text-field--outlined mdc-text-field--with-leading-icon">
        <i class="material-icons mdc-text-field__icon" tabindex="0" role="button">person</i>
        <input name="username" type="text" id="username-text-field" class="mdc-text-field__input" required>
        <div class="mdc-notched-outline">
            <div class="mdc-notched-outline__leading"></div>
            <div class="mdc-notched-outline__notch">
                <label for="username-text-field" class="mdc-floating-label">Username</label>
            </div>
            <div class="mdc-notched-outline__trailing"></div>
        </div>
        <div class="mdc-line-ripple"></div>
    </div>
    <div class="mdc-text-field-helper-line" style="padding-bottom: 5px">
        <div class="mdc-text-field-helper-text">Ask Sir Dan, or tignan mo na lang sa FB Page LOL</div>
    </div>
    <div id="password"
        class="mdc-text-field mdc-text-field--with-leading-icon mdc-text-field--outlined mdc-text-field--with-trailing-icon">
        <i class="material-icons mdc-text-field__icon" role="button">vpn_key</i>
        <input name="password" type="text" id="password-text-field" class="mdc-text-field__input" required>
        <i id="password-toggler" class="material-icons mdc-text-field__icon" tabindex="0" role="button">visibility</i>
        <div class="mdc-notched-outline">
            <div class="mdc-notched-outline__leading"></div>
            <div class="mdc-notched-outline__notch">
                <label for="password-text-field" class="mdc-floating-label">Password</label>
            </div>
            <div class="mdc-notched-outline__trailing"></div>
        </div>
        <div class="mdc-line-ripple"></div>
    </div>
    <div class="mdc-text-field-helper-line" style="padding-bottom: 5px">
        <div class="mdc-text-field-helper-text">Sure ka ba jan.</div>
    </div>
    <button id="auth-button" class="mdc-button mdc-button--outlined">
        <span class="mdc-button__label">Sign In</span>
    </button>
</form>

<!-- Snackbars -->
<div id="wrong-credentials" class="mdc-snackbar">
  <div class="mdc-snackbar__surface">
    <div class="mdc-snackbar__label" role="status" aria-live="polite"> Wrong credentials.</div>
    <div class="mdc-snackbar__actions">
      <button id="credentials-hint__button" type="button" class="mdc-button mdc-snackbar__action">Hint</button>
      <button class="mdc-icon-button mdc-snackbar__dismiss material-icons">close</button>
    </div>
  </div>
</div>

<!-- Dialogs -->
<div id="credentials-hint" class="mdc-dialog">
  <div class="mdc-dialog__container">
    <div class="mdc-dialog__surface">
      <h2 class="mdc-dialog__title">Credentials Hint</h2>
      <div class="mdc-dialog__content">
        <h2><b>Username: </b>dan@slis.upd.edu.ph</h2>
        <h2><b>Password: </b>O2kdF29k1</h2>
      </div>
    </div>
  </div>
  <div class="mdc-dialog__scrim"></div>
</div>
