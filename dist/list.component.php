<?php

$_SESSION['authenticated'] = true;

?>


<div id="new-filter" class="mdc-text-field mdc-text-field--with-trailing-icon" style="display: none;">
    <input id="new-filter__field" type="text" class="mdc-text-field__input">
    <label class="mdc-floating-label" for="my-text-field">Just type something, ;) Case-sensitive?
        <span>No</span>.</label>
    <i id="case-sensitivity_toggle" class="material-icons mdc-text-field__icon" tabindex="0"
        role="button">text_fields</i>
    <div class="mdc-line-ripple"></div>
</div>

<table id="list">
    <thead>
        <tr>
            <th class="mdc-typography--headline5">NAME</th>
            <th class="mdc-typography--headline5">CREATOR</th>
            <th class="mdc-typography--headline5">TYPE</th>
        </tr>
    </thead>

    <tbody>

    </tbody>
</table>

<div id="resource-modifiers">
    <button id="edit-resource__button" class="mdc-fab" aria-label="Edit resource" style="display: none;">
        <span class="mdc-fab__icon material-icons">mode_edit</span>
    </button>

    <button id="delete-resource__button" class="mdc-fab mdc-fab--extended" aria-label="Delete resource"
        style="display: none; height: 56px;">
        <span class="mdc-fab__label"></span>
        <span class="mdc-fab__icon material-icons">delete</span>
    </button>

    <button id="new-resource__button" class="mdc-fab" aria-label="New resource">
        <span class="mdc-fab__icon material-icons">add</span>
    </button>
</div>

<div id="new-resource__dialog" class="mdc-dialog" role="alertdialog" aria-modal="true" aria-labelledby="my-dialog-title"
    aria-describedby="my-dialog-content">
    <div class="mdc-dialog__container">
        <div class="mdc-dialog__surface">
            <h2 class="mdc-dialog__title">New Resource</h2>
            <div class="mdc-dialog__content">
                <form id="new-resource__form">
                    <div id="nr__name" class="mdc-text-field">
                        <input name="name" type="text" id="nr__name-text-field" class="mdc-text-field__input" required>
                        <label class="mdc-floating-label" for="nr__name-text-field">Name</label>
                        <div class="mdc-line-ripple"></div>
                    </div>
                    <br>
                    <div id="nr__creator" class="mdc-text-field">
                        <input name="creator" type="text" id="nr__creator-text-field" class="mdc-text-field__input"
                            required>
                        <label class="mdc-floating-label" for="nr__creator-text-field">Creator</label>
                        <div class="mdc-line-ripple"></div>
                    </div>
                    <br>
                    <div id="nr__type" class="mdc-text-field">
                        <input name="type" type="text" id="nr__type-text-field" class="mdc-text-field__input" required>
                        <label class="mdc-floating-label" for="nr__type-text-field">Type</label>
                        <div class="mdc-line-ripple"></div>
                    </div>
                </form>
            </div>
            <footer class="mdc-dialog__actions">
                <button type="button" class="mdc-button mdc-dialog__button" data-mdc-dialog-action="no">
                    <span class="mdc-button__label">Cancel</span>
                </button>
                <button id="new-resource__submit" type="button"
                    class="mdc-button mdc-dialog__button mdc-dialog__button--default" data-mdc-dialog-action="yes" disabled>
                    <span class="mdc-button__label">Add</span>
                </button>
            </footer>
        </div>
    </div>
    <div class="mdc-dialog__scrim"></div>
</div>

<div id="edit-resource__dialog" class="mdc-dialog" role="alertdialog" aria-modal="true"
    aria-labelledby="my-dialog-title" aria-describedby="my-dialog-content">
    <div class="mdc-dialog__container">
        <div class="mdc-dialog__surface">
            <h2 class="mdc-dialog__title">Edit Resource</h2>
            <div class="mdc-dialog__content">
                <form id="edit-resource__form">
                    <div id="er__name" class="mdc-text-field">
                        <input name="name" type="text" id="er__name-text-field" class="mdc-text-field__input" value=" "
                            required>
                        <label class="mdc-floating-label" for="er__name-text-field">Name</label>
                        <div class="mdc-line-ripple"></div>
                    </div>
                    <br>
                    <div id="er__creator" class="mdc-text-field">
                        <input name="creator" type="text" id="er__creator-text-field" class="mdc-text-field__input"
                            value=" " required>
                        <label class="mdc-floating-label" for="er__creator-text-field">Creator</label>
                        <div class="mdc-line-ripple"></div>
                    </div>
                    <br>
                    <div id="er__type" class="mdc-text-field">
                        <input name="type" type="text" id="er__type-text-field" class="mdc-text-field__input" value=" "
                            required>
                        <label class="mdc-floating-label" for="er__type-text-field">Type</label>
                        <div class="mdc-line-ripple"></div>
                    </div>
                </form>
            </div>
            <footer class="mdc-dialog__actions">
                <button type="button" class="mdc-button mdc-dialog__button" data-mdc-dialog-action="no">
                    <span class="mdc-button__label">Cancel</span>
                </button>
                <button id="edit-resource__submit" type="button"
                    class="mdc-button mdc-dialog__button mdc-dialog__button--default" data-mdc-dialog-action="yes" disabled>
                    <span class="mdc-button__label">Update</span>
                </button>
            </footer>
        </div>
    </div>
    <div class="mdc-dialog__scrim"></div>
</div>